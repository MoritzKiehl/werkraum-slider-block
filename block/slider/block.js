import { InnerBlocks } from '@wp/editor'; // or wp.editor
(function (editor, components, i18n, element) {
    var el = element.createElement;
    var registerBlockType = wp.blocks.registerBlockType;
    var RichText = wp.editor.RichText;
    var BlockControls = wp.editor.BlockControls;
    var AlignmentToolbar = wp.editor.AlignmentToolbar;
    var InspectorControls = wp.editor.InspectorControls;
    var TextControl = wp.components.TextControl;
    var InnerBlock = wp.editor;

    registerBlockType('werkraum/slider-block', { // The name of our block. Must be a string with prefix. Example: my-plugin/my-custom-block.
        title: 'Slider', // The title of our block.
        description: 'A custom block to display a media galery as slider.', // The description of our block.
        icon: 'universal-access-alt', // Dashicon icon for our block. Custom icons can be added using inline SVGs.
        category: 'common', // The category of the block.
        attributes: { // Necessary for saving block content.
            title: {
                type: 'array',
                source: 'children',
                selector: 'h3',
            },
            subtitle: {
                type: 'array',
                source: 'children',
                selector: 'h5',
            },
            bio: {
                type: 'array',
                source: 'children',
                selector: 'p',
            },
        },

        edit: function (props) {

            var attributes = props.attributes;
            var alignment = props.attributes.alignment;


            return [
                el(BlockControls, {key: 'controls'}, // Display controls when the block is clicked on.
                    el('div', {className: 'components-toolbar'},
                        el(MediaUpload, {
                            onSelect: onSelectImage,
                            type: 'image',
                            render: function (obj) {
                                return el(components.Button, {
                                        className: 'components-icon-button components-toolbar__control',
                                        onClick: obj.open
                                    },
                                    el('svg', {className: 'dashicon dashicons-edit', width: '20', height: '20'},
                                        el('path', {d: "M2.25 1h15.5c.69 0 1.25.56 1.25 1.25v15.5c0 .69-.56 1.25-1.25 1.25H2.25C1.56 19 1 18.44 1 17.75V2.25C1 1.56 1.56 1 2.25 1zM17 17V3H3v14h14zM10 6c0-1.1-.9-2-2-2s-2 .9-2 2 .9 2 2 2 2-.9 2-2zm3 5s0-6 3-6v10c0 .55-.45 1-1 1H5c-.55 0-1-.45-1-1V8c2 0 3 4 3 4s1-3 3-3 3 2 3 2z"})
                                    )
                                );
                            }
                        })
                    ),
                ),
                el('div', {className: props.className},
                    el('div', {
                            className: 'organic-profile-content', style: {textAlign: alignment}
                        },
                        el(RichText, {
                            tagName: 'p',
                            placeholder: 'Informationstext für Slider',
                            keepPlaceholderOnFocus: true,
                            value: attributes.bio,
                            onChange: function (newBio) {
                                props.setAttributes({bio: newBio});
                            },
                        }),
                    ),
                )
            ];        },

        save: function (props) {
            var attributes = props.attributes;
            var alignment = props.attributes.alignment;
            return (
                el('div', {
                        className: props.className
                    },
                    el('div', {
                            className: 'slider-text-overlay',
                            style: {textAlign: attributes.alignment}
                        },
                        el(RichText.Content, {
                            tagName: 'p',
                            value: attributes.bio
                        }),
                    )
                )
            );
        },
    });

})(
    window.wp.editor,
    window.wp.components,
    window.wp.i18n,
    window.wp.element,
);
