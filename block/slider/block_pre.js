/**
 * Block dependencies
 */
import classnames from 'classnames';

const { __ } = wp.i18n;

const { Fragment } = wp.element;

const { registerBlockType } = wp.blocks;

const { InnerBlocks } = wp.editor;

/**
 * Register example block
 */
export default registerBlockType(
    'hmsblocks/hms-layout-1',
    {
        title: __( 'HMS Layout', 'hmsblocks' ),
        description: __( 'Custom Column Block.', 'hmsblocks'),
        category: 'layout',
        icon: 'columns',
        keywords: [
            __( 'Columns', 'hmsblocks' ),
        ],
        attributes: {
        },

        edit: props => {

            const { attributes: { placeholder },
                className, setAttributes,  } = props;

            return (
                <div className={ className }>

                    <InnerBlocks
                        layouts={ [
                            { name: 'column-1', label: 'Column 1', icon: 'columns' },
                        ] }
                    />

                </div>
            );
        },
        save: props => {
            const { attributes: { className }, setAttributes } = props;
            return (
                <div className={ className }>
                    <InnerBlocks.Content />
                </div>
            );
        },
    },
);